import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Personne } from 'src/app/classes/personne';
import { PersonneService } from 'src/app/services/personne.service';
import { Router } from '@angular/router';
import { PersonneDetailsComponent } from '../personne-details/personne-details.component';


@Component({
  selector: 'app-personne-list',
  templateUrl: './personne-list.component.html',
  styleUrls: ['./personne-list.component.css']
})
export class PersonneListComponent implements OnInit {
 personnes: Observable<Personne[]>;
  constructor(private personneService: PersonneService, private router: Router) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData(){
    this.personnes = this.personneService.getPersonnesList();
  }

  deletePersonne(num: BigInteger){
    this.personneService.deletePersonne(num)
        .subscribe(
          data => {
            console.log(data);
            this.reloadData();
          },
          error => console.log(error));    
  }

  personneDetails(num: BigInteger){
    this.router.navigate(['personnes/details', num]);
  }

  updatePersonne(num: BigInteger){
    this.router.navigate(['personnes/update', num]);
  }
}
