import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/classes/personne';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-update-personne',
  templateUrl: './update-personne.component.html',
  styleUrls: ['./update-personne.component.css']
})
export class UpdatePersonneComponent implements OnInit {
  submitted = false;
  personne: Personne;
  num: BigInteger;
  constructor(private router: Router, private route: ActivatedRoute, private personneService: PersonneService) { }

  ngOnInit(): void {
    this.personne = new Personne();
    this.num = this.route.snapshot.params['num'];
    this.submitted = false;
    this.personneService.getPersonne(this.num)
    .subscribe(data => {
        console.log(data);
        this.personne = data;
    }, error => console.log(error));
  }

  updatePersonne(){
    this.personneService.updatePersonne(this.num, this.personne)
        .subscribe(data => console.log(data), error => console.log(error));
        this.personne = new Personne();
        this.gotoList();
  }
  onSubmit() {
    this.updatePersonne();
    this.submitted = true;
  }
  gotoList() {
    this.router.navigate(['/personnes/update']);
  }


}
