import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/classes/personne';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-personne-details',
  templateUrl: './personne-details.component.html',
  styleUrls: ['./personne-details.component.css']
})
export class PersonneDetailsComponent implements OnInit {
 personne: Personne;
 num: BigInteger;

  constructor(private route: ActivatedRoute, private router: Router, private personneService: PersonneService) { }

  ngOnInit(): void {
    this.personne = new Personne();
    this.num = this.route.snapshot.params['num'];
    this.personneService.getPersonne(this.num)
    .subscribe(data => {
      console.log(data);
      this.personne = data;
    }, error => console.log(error));
  }
  list(){
    this.router.navigate(['personnes']);
  }

}
