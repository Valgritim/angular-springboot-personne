import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonneListComponent } from './composants/personne-list/personne-list.component';
import { CreatePersonneComponent } from './composants/create-personne/create-personne.component';
import { UpdatePersonneComponent } from './composants/update-personne/update-personne.component';
import { PersonneDetailsComponent } from './composants/personne-details/personne-details.component';


const routes: Routes = [
  { path: '', redirectTo: 'personnes', pathMatch: 'full'},
  { path: 'personnes', component: PersonneListComponent},
  { path: 'personnes/add', component: CreatePersonneComponent},
  { path: 'personnes/update', component: UpdatePersonneComponent},
  { path: 'personnes/details', component: PersonneDetailsComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
